<?php

function sum($ar) {
	
    $t = 0;

    //create this to store extra result
	$s = 0;             

    //remove the: => $val
    //then the foreach loop runs correctly
    foreach ($ar as $ele) {
		
        if(is_array($ele)) {
			
            //assign sum to new $s var
            $s = sum($ele);
			
        } else {
            $t = $ele + $t;
	
        }
    }

    //add result of all iterations ($t) + the sum of $example[2] ($s) 
    return $t + $s;
}

$example = array(1, 2, array(10,20,30), 4);

echo sum($example); // Outputs 67


?>